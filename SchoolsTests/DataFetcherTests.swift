//
//  DataFetcherTests.swift
//  SchoolsTests
//
//  Created by Hemanth Prasad on 4/4/22.
//

import XCTest
@testable import NYCSchools

class DataFetcherTests: XCTestCase {

    var fetcher: DataFetcher!


    override func setUpWithError() throws {
        fetcher = DataFetcher(apiService: APIService(httpService: MockHTTPService()))
    }

    override func tearDownWithError() throws {
        fetcher = nil
    }

    func testFetchData() async throws {
        let schools = try await fetcher.fetchSchoolData()
        XCTAssertEqual("Clinton School Writers & Artists, M.S. 260", schools.first?.name)
        XCTAssertNil(schools.first?.satScore)

        XCTAssertEqual("School for Legal Studies", schools.last?.name)
        XCTAssertEqual("396", schools.last?.satScore?.satMatchScore)
        XCTAssertEqual("413", schools.last?.satScore?.satReadingScore)
        XCTAssertEqual("395", schools.last?.satScore?.satWritingScore)

        XCTAssert(schools.count > 0)
    }
}
