//
//  SchoolListModelTests.swift
//  SchoolsTests
//
//  Created by Hemanth Prasad on 4/4/22.
//

import XCTest
@testable import NYCSchools

class SchoolListModelTests: XCTestCase {

    var viewModel: SchoolsListViewModel!

    @MainActor
    override func setUpWithError() throws {
        viewModel = SchoolsListViewModel(dataProvider: .init(apiService: .init(httpService: MockHTTPService())))
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }

    @MainActor
    func testFailingState() async throws {
        viewModel = SchoolsListViewModel(dataProvider: .init(apiService: .init(httpService: FailingHTTPService())))
        XCTAssertEqual(self.viewModel.state, .notLoaded)
        let result = await viewModel.loadData()
        viewModel.updateState(result: result)
        XCTAssertEqual(self.viewModel.state, .failed(NSError(domain: "Some Error", code: 0)))
    }

    @MainActor
    func testSuccessState() async throws {
        viewModel = SchoolsListViewModel(dataProvider: .init(apiService: APIService(httpService: MockHTTPService())))
        let result = await viewModel.loadData()
        viewModel.updateState(result: result)
        if case .loaded = viewModel.state {
            XCTAssert(true)
        } else {
            XCTAssert(false)
        }
    }
}

class FailingHTTPService: HTTPService {
    func data(url: URL) async throws -> (Data, URLResponse) {
        throw NSError(domain: "Some Error", code: 0)
    }
}
