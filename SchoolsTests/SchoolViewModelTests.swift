//
//  SchoolViewModelTests.swift
//  SchoolsTests
//
//  Created by Hemanth Prasad on 4/5/22.
//

import XCTest
@testable import NYCSchools

class SchoolViewModelTests: XCTestCase {

    typealias Section = SchoolViewModel.Section
    typealias Row = SchoolViewModel.Row

    var viewModel: SchoolViewModel!
    var school: School!

    override func setUp() async throws {
        let dataFetcher = DataFetcher(apiService: APIService(httpService: MockHTTPService()))
        let data = try! await dataFetcher.fetchSchoolData()
        school = data.last!
        viewModel = await .init(school: school)
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }

    @MainActor func testExample() throws {
        let sections = viewModel.makeSections()
        XCTAssertEqual(sections[0], Section(id: "DBN", nil, { Row.pair("School DBN", "14K477") }))
        XCTAssertEqual(sections[1], Section(id: "SAT Score", "SAT Score", {
            Row.pair("SAT Writing Avg. Score", "395")
            Row.pair("SAT Critical Reading Avg. Score", "413")
            Row.pair("SAT Math Avg. Score", "396")
            Row.pair("Num of SAT Test Takers", "134")})
        )

        XCTAssertEqual(sections[2], Section(id: "OverView", "OverView", { Row.expandableText("Our mission is to guide students to become critical thinkers, independent learners, and productive citizens. Founded in 1996, we are a school in which studentsÂ’ voices are heard, character is developed, excellence prevails, and parents are our partners. Using critical thinking skills, technology, and reading and writing skills, our students investigate and explore educational and career opportunities in the legal field. The families of our students are actively engaged in our culture and are encouraged to chaperone our students on overnight and weekend trips such as college tours and historical sites of interest.") }))


        XCTAssertEqual(sections[3], Section(id: "Academic Opportunities", "Academic Opportunities", {
            Row.text("AVID College and Career Program (Advancement Via Individual Determination)")
            Row.text("CTE program(s) in: Law and Public Safety")
            Row.text("Computer Forensic ProgramÂ—Cyber Academy")
            Row.text("Microsoft IT Academy Certification Program")
            Row.text("iLearnNYC: Program for expanded online coursework and self-paced learning")
            })
        )

        XCTAssertEqual(sections[4], Section(id: "Others", "Others", {
            Row.pair("Location", "850 Grand Street, Brooklyn NY 11211 (40.711134, -73.938921)")
            Row.pair("Phone Number", "718-387-2800")
            Row.pair("Fax Number", "718-387-3281")
            Row.pair("School Email", "mail@thesls.net")
            Row.pair("School website", "www.thesls.net")
            Row.pair("Subway", "L to Grand St")})
        )
    }
}
