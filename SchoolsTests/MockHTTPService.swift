//
//  MockApiService.swift
//  NYCSchools
//
//  Created by Hemanth Prasad on 4/8/22.
//

import Foundation
@testable import NYCSchools

class MockHTTPService: HTTPService {
    
    let mocks = [DataFetcher.schoolsURL : "schools.json",
                 DataFetcher.satURL: "sat.json"]
    
    func data(url: URL) async throws -> (Data, URLResponse) {
        guard let fileName = mocks.first(where: { $0.key == url })?.value,
              let fileURL = Bundle(for: MockHTTPService.self).url(forResource: fileName,
                                                                  withExtension: nil) else {
            throw NSError(domain: "Mock file not found", code: 0)
        }
        
        return (try Data(contentsOf: fileURL), URLResponse())
    }
}
