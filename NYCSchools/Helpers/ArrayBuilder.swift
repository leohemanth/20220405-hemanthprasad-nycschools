//
//  ArrayBuilder.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import Foundation

@resultBuilder
public enum ArrayBuilder<Element> {
    public typealias Component = [Element]

    public static func buildExpression(_ expression: Element?) -> Component {
        expression.map { [$0] } ?? []
    }

    public static func buildExpression(_ expression: [Element]...) -> Component {
        expression.flatMap { $0 }
    }

    public static func buildBlock(_ children: Component...) -> Component {
        children.flatMap { $0 }
    }

    public static func buildOptional(_ children: Component?) -> Component {
        children ?? []
    }

    public static func buildEither(first child: Component) -> Component {
        child
    }
}
