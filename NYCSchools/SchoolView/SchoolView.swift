//
//  SchoolView.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import SwiftUI

struct SchoolView: View {
    let schoolViewModel: SchoolViewModel

    var body: some View {
        List {
            Section {
                Text(schoolViewModel.school.name)
                    .font(.title)
                    .bold()
                    .multilineTextAlignment(.leading)
                    .fixedSize(horizontal: false, vertical: true)
            }
            ForEach(schoolViewModel.makeSections()) { section in
                Section(header: Text(section.title ?? "").bold()) {
                    ForEach(section.rows) { row in
                        switch row {
                        case let .text(text):
                            Text(text)
                        case let .expandableText(text):
                            ExpandableView(text: text)
                        case let .pair(title, desc):
                            RowView(title, desc)
                        }
                    }
                }
            }
        }
    }
}

struct RowView: View {
    let title: String
    let desc: String

    init(_ title: String, _ desc: String) {
        self.title = title
        self.desc = desc
    }

    var body: some View {
        HStack {
            Text(title)
            Spacer(minLength: 80)
            Text(desc).multilineTextAlignment(.trailing)
        }
    }
}

struct ExpandableView: View {
    let text: String

    @State var expanded = false

    var body: some View {
        Text(text)
            .frame(maxHeight: expanded ? .greatestFiniteMagnitude : 100)
            .onTapGesture {
                expanded.toggle()
            }
    }
}
