//
//  SchoolViewModel.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import Foundation

@MainActor
class SchoolViewModel {
    let school: School

    init(school: School) {
        self.school = school
    }

    struct Section: Identifiable, Equatable {
        let id: String
        let title: String?
        let rows: [Row]

        init(id: String, _ title: String? = nil, @ArrayBuilder<Row> _ makeRows: () -> [Row]) {
            self.title = title
            self.rows = makeRows()
            self.id = id
        }
    }

    enum Row: Identifiable, Equatable {
        case text(String)
        case expandableText(String)
        case pair(String, String)

        var id: String {
            switch self {
            case let .text(text):
                return text
            case let .expandableText(text):
                return text
            case let .pair(title, text):
                return title + text
            }
        }

        static func makePair(_ title: String, _ dec: String?) -> Row? {
            dec.map { Row.pair(title, $0) }
        }
    }

    func makeSettings(@ArrayBuilder<Section> _ sections: () -> [Section]) -> [Section] {
        sections().filter { $0.rows.count != 0 }
    }

    func makeSections() -> [Section] {
        return makeSettings {
            Section(id: "DBN") {
                Row.pair("School DBN", school.dbn)
            }
            if let satScore = school.satScore {
                Section(id: "SAT Score", "SAT Score") {
                    Row.pair("SAT Writing Avg. Score", satScore.satWritingScore)
                    Row.pair("SAT Critical Reading Avg. Score", satScore.satReadingScore)
                    Row.pair("SAT Math Avg. Score", satScore.satMatchScore)
                    Row.pair("Num of SAT Test Takers", satScore.numOfSATTestTakers)
                }
            }
            if let overView = school.overview {
                Section(id: "OverView", "OverView" , { Row.expandableText(overView) })
            }
            Section(id: "Academic Opportunities", "Academic Opportunities") {
                school.academicOpportunities.sorted().map { Row.text($0) }
            }
            Section(id:"Others", "Others") {
                Row.makePair("Location", school.location)
                Row.makePair("Phone Number", school.phoneNumber)
                Row.makePair("Fax Number", school.faxNumber)
                Row.makePair("School Email", school.schoolEmail)
                Row.makePair("School website", school.website)
                Row.makePair("Subway", school.subway)
            }
        }
    }
}
