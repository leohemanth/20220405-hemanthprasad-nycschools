//
//  Model.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import Foundation

enum Response {
    struct SAT {
        let jsonData: [String: Any]

        subscript(key: CodingKeys) -> String? {
            get {
                if let data = jsonData[key.rawValue] as? String {
                    return data
                } else if let data = jsonData[key.rawValue] as? Int {
                    return String(data)
                }
                return nil
            }
        }

        enum CodingKeys: String, CodingKey {
            case dbn = "DBN"
            case schoolName = "SCHOOL NAME"
            case numOfSATTestTakers = "Num of SAT Test Takers"
            case satCriticalReadingAvgScore = "SAT Critical Reading Avg. Score"
            case satMathAvgScore = "SAT Math Avg. Score"
            case satWritingAvgScore = "SAT Writing Avg. Score"
        }
    }

    // MARK: - School
    struct SchoolData {
        let jsonData: [String: Any]

        subscript(key: CodingKeys) -> String? {
            get {
                if let data = jsonData[key.rawValue] as? String {
                    return data
                } else if let data = jsonData[key.rawValue] as? Int {
                    return String(data)
                }
                return nil
            }
        }
        
        enum CodingKeys: String, CodingKey {
            case dbn
            case schoolName = "school_name"
            case boro
            case overviewParagraph = "overview_paragraph"
            case location
            case phoneNumber = "phone_number"
            case faxNumber = "fax_number"
            case schoolEmail = "school_email"
            case website, subway, bus
        }
    }
}
