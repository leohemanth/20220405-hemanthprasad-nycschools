//
//  APIService.swift
//  NYCSchools
//
//  Created by Hemanth Prasad on 4/8/22.
//

import Foundation

protocol HTTPService {
    func data(url: URL) async throws -> (Data, URLResponse)
}

class APIService {
    let httpService: HTTPService
    
    init(httpService: HTTPService) {
        self.httpService = httpService
    }
    
    func fetch(url: URL) async throws -> Data {
        let (data, _) = try await httpService.data(url: url)
        return data
    }
}

extension URLSession: HTTPService {
    func data(url: URL) async throws -> (Data, URLResponse) {
        try await URLSession.shared.data(from: url)
    }
}
