//
//  APIService.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import Foundation

class DataFetcher {

    let apiService: APIService

    init(apiService: APIService) {
        self.apiService = apiService
    }
    
    static let schoolsURL = URL(string:"https://drive.google.com/u/0/uc?id=1SlY0rPdeD3LYVAy9SNjm7dBohbb0b1th&export=download")!
    static let satURL = URL(string:"https://drive.google.com/u/0/uc?id=1C4kWcWc3NTEUdAodjGnoMrWP_SNCglYJ&export=download")!
    
    func fetchSchoolData() async throws -> [School] {
        async let schoolData: [Response.SchoolData] = (try fetchJson(url: DataFetcher.schoolsURL))
            .map { .init(jsonData: $0) }
        async let satData: [Response.SAT] = (try fetchJson(url: DataFetcher.satURL))
            .map { .init(jsonData: $0) }
        // Creating hashtable for easy lookup of sat score
        let satHash = try await makeHash(with: satData)
        return try await schoolData.compactMap { try .init(data: $0, satHash: satHash) }
    }

    // Assuming dbn is always present
    func makeHash(with json: [Response.SAT]) async throws -> [String: Response.SAT] {
        return try Dictionary(uniqueKeysWithValues: json.compactMap {
            guard let k = $0[.dbn] else { throw NSError(domain: "dbn not found", code: 0, userInfo: ["data": $0]) }
            return (k, $0)
        })
    }

    // Parsing data as JSON Dictionary for easy look up
    func fetchJson(url: URL) async throws -> [[String: Any]] {
        let data = try await apiService.fetch(url: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
            throw NSError(domain: "JSON Type mismatch", code: 0)
        }
        return json
    }
}

extension School {
    init?(data: Response.SchoolData, satHash: [String : Response.SAT]) throws {
        guard let dbn = data[.dbn] else { throw NSError(domain: "dbn not found", code: 0)}
        let academicOpportunityKeys: [String] = data.jsonData.keys.filter { $0.contains("academicopportunities") }

        self.init(dbn: dbn,
                  name: data[.schoolName],
                  satScore: try .init(response: satHash[dbn] ),
                  overview: data[.overviewParagraph],
                  academicOpportunities: academicOpportunityKeys.compactMap { data.jsonData[$0] as? String }.filter { !$0.isEmpty },
                  location: data[.location],
                  phoneNumber: data[.phoneNumber],
                  faxNumber: data[.faxNumber],
                  schoolEmail: data[.schoolEmail],
                  website: data[.website],
                  subway: data[.subway]
        )
    }
}

extension SATScore {
    init?(response: Response.SAT?) throws {
        guard let response = response else { return nil }
        guard let math = response[.satMathAvgScore],
              let reading = response[.satCriticalReadingAvgScore],
              let writing = response[.satWritingAvgScore],
              let number = response[.numOfSATTestTakers] else {
            throw NSError(domain: "SATSCORE not found", code: 0, userInfo: ["response": response])
        }
        self.init(satMatchScore: math,
                  satReadingScore: reading,
                  satWritingScore: writing,
                  numOfSATTestTakers: number)
    }
}
