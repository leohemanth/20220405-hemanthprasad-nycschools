//
//  Model.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import Foundation

struct School: Equatable, Identifiable {
    let name: String
    let dbn: String
    let overview: String?
    let academicOpportunities: [String]
    let satScore: SATScore?

    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let subway: String?

    var id: String { name }
    
    init?(dbn: String,
          name: String?,
          satScore: SATScore?,
          overview: String?,
          academicOpportunities: [String],
          location: String?,
          phoneNumber: String?,
          faxNumber: String?,
          schoolEmail: String?,
          website: String?,
          subway: String?) {
        guard let name = name else { return nil}
        self.name = name
        self.dbn = dbn
        self.satScore = satScore
        self.overview = overview
        self.academicOpportunities = academicOpportunities
        self.location = location
        self.phoneNumber = phoneNumber
        self.faxNumber = faxNumber
        self.schoolEmail = schoolEmail
        self.website = website
        self.subway = subway
    }
}

struct SATScore: Equatable {
    let satMatchScore: String
    let satReadingScore: String
    let satWritingScore: String
    let numOfSATTestTakers: String
}
