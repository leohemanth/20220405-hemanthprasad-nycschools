//
//  ContentView.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import SwiftUI

struct SchoolListView: View {
    @ObservedObject var viewModel: SchoolsListViewModel
    @State var searchText: String = ""

    init(viewModel: SchoolsListViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        switch viewModel.state {
        case let .loaded(schools):
            NavigationView {
                List(filteredSchools(query: searchText, schools: schools)) { school in
                    NavigationLink(destination: SchoolView(schoolViewModel: .init(school: school))) {
                        Text(.init(highlightedText(school: school)))
                    }
                }.searchable(text: $searchText)
                    .navigationBarTitle("Schools")
            }
        case .loading:
            ProgressView()
        case .notLoaded:
            Button("Reload") {
                viewModel.reloadData()
            }.task {
                viewModel.reloadData()
            }
        case .failed:
            Button("Something went wrong, Try again") {
                viewModel.reloadData()
            }
        }
    }

    // Emphasis searchText in the results
    func highlightedText(school: School) -> String {
        return school.name.replacingOccurrences(of: searchText, with: "**"+searchText+"**")
    }

    func filteredSchools(query: String, schools: [School]) -> [School] {
        guard !query.isEmpty else { return schools }
        return schools.filter { $0.name.lowercased().contains(query.lowercased()) }
    }
}
