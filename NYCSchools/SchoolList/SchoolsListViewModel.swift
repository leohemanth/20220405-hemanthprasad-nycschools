//
//  SchoolsListViewModel.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import Foundation

@MainActor
class SchoolsListViewModel: ObservableObject {
    let dataProvider: DataFetcher

    enum State: Equatable {
        case notLoaded
        case loading
        case failed(NSError)
        case loaded([School])
    }

    @Published var state: State = .notLoaded

    init(dataProvider: DataFetcher) {
        self.dataProvider = dataProvider
    }

    func loadData() async -> Result<[School], Error> {
        do {
            let schools = try await self.dataProvider.fetchSchoolData()
            return .success(schools)
        } catch {
            return .failure(error as NSError)
        }
    }

    func reloadData() {
        state = .loading
        Task.detached(priority: .userInitiated) {
            let result = await self.loadData()
            await self.updateState(result: result)
        }
    }

    @MainActor
    func updateState(result: Result<[School], Error>) {
        switch result {
        case let .success(schools):
            state = .loaded(schools)
        case let .failure(error):
            state = .failed(error as NSError)
        }
    }
}
