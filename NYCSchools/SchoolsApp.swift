//
//  SchoolsApp.swift
//  Schools
//
//  Created by Hemanth Prasad on 4/4/22.
//

import SwiftUI

@main
struct SchoolsApp: App {
    let apiService = APIService(httpService: URLSession.shared)
    
    var body: some Scene {
        WindowGroup {
            SchoolListView(viewModel: .init(dataProvider: .init(apiService: apiService)))
        }
    }
}
